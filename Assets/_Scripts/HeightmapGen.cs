﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System;

public class HeightmapGen : MonoBehaviour
{

    private float[,] HeightMap;
    private float[] weights = { 1f, 1/2f, 1/4f, 1/8f, 1/16f, 1/32f};//, 0.2f, 4f, 0.5f, 3f }; //new float[30];
    private float[] frequencies = { 1f, 2f, 4f, 8f, 16f, 32f};// 2f, 4f, 8f, 16f, 32f }; //new float[30];
    private int xTerrainRes;
    private int yTerrainRes;
    private GameObject myTerrain;
    private TerrainData tData;
    public float xOrg = 0;
    public float yOrg = 0;
    public float frequency = 1.0F;
    private float previousFrequency = 0.0F;
    private int i = 0;
    private int j = 0;
    public Texture2D terrainTex;
    public int passes = 2;
    public float amplitude = 1.0F;  //terrain height
    private float previousAmplitude = 1.0F;

    // Use this for initialization
    void Start()
    {
        previousFrequency = frequency;
        previousAmplitude = amplitude;
        generateNewTerrain();
    }

    void generateNewTerrain()
    {
        //set the data of the terrain we will create
        tData = new TerrainData();
        tData.size = new Vector3(32, 500, 32);
        tData.heightmapResolution = 513;
        tData.SetDetailResolution(512, 8);
        HeightMap = tData.GetHeights(0, 0, tData.heightmapWidth, tData.heightmapHeight); //set the HeightMap tab to make it correspond to the actual height tab for our terrain



        for (int i = 0; i < passes; i++)
        {
            int y = 0;
            while (y < tData.heightmapHeight)
            {
                int x = 0;
                while (x < tData.heightmapWidth)
                {
                    float xCoord = Convert.ToSingle(x) / Convert.ToSingle(tData.heightmapWidth - 1); //need to convert everything to float in order for the Perling Noise function to work
                    float yCoord = Convert.ToSingle(y) / Convert.ToSingle(tData.heightmapHeight - 1); // same
                    float sample = Mathf.PerlinNoise(xCoord * frequencies[i] * frequency, yCoord * frequencies[i] * frequency); //generate height value for a vertex of the terrain with the Perlin Noise function
                    HeightMap[x, y] += sample * weights[i] * amplitude; //put the generated value in the corresponding table of heights
                    x++;
                }
                y++;
            }
        }

        int p = 0;
        while (p < tData.heightmapHeight)
        {
            int x = 0;
            while (x < tData.heightmapWidth)
            {
                HeightMap[x, p] = HeightMap[x, p] / SumWeights(); //put the generated value in the corresponding table of heights
                x++;
            }
            p++;
        }

        tData.SetHeights(0, 0, HeightMap); //put the generated heights into our terrain data

        //set the texture of the terrain
        SplatPrototype[] tex = new SplatPrototype[1];
        tex[0] = new SplatPrototype();
        tex[0].texture = terrainTex;
        tex[0].tileSize = new Vector2(1, 1);
        tData.splatPrototypes = tex;

        //create the terrain data object as an asset
        AssetDatabase.CreateAsset(tData, "Assets/testTerrain.asset");

        //create the terrain
        myTerrain = Terrain.CreateTerrainGameObject(tData);
        myTerrain.transform.position = new Vector3(-250f, 0f, -250f);

    }

    // Update is called once per frame
    void Update()
    {
        //if the scale has changed, then destroy the previous terrain and generate a new one with the current scale.
        if (frequency != previousFrequency || amplitude != previousAmplitude)
        {
            Destroy(myTerrain);
            generateNewTerrain();
            previousFrequency = frequency;
            previousAmplitude = amplitude;
        }

    }

    float SumWeights()
    {
        float sumWeights = 0f;
        for (int i = 0; i < weights.Length; i++)
        {
            sumWeights += weights[i];
        }

        return sumWeights;
    }
}