# Procedural terrain generation
This project is a Unity editor tool to help create terrains.

## why ?
I made this project to practice developping editor tools and also to learn how terrains work in Unity.
This project also made me learn a lot about procedural generation and Perlin Noise function.